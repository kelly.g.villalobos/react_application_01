import * as React from 'react';
import {Route, Routes} from 'react-router-dom';
import './App.css';
import TopAppBar from './components/AppBar/TopAppBar';
import Home from './pages/Home/Home';
import Contact from './pages/Contact/Contact';
import About from './pages/About/About';

export function App() {
  return (
    <div>
      <TopAppBar />
      <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/contact" element={<Contact />} />
      </Routes>
    </div>
  );
}

export default App;
