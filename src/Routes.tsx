import React from 'react';
import About from './pages/About/About';
import Home from './pages/Home/Home';
import Contact from './pages/Contact/Contact';

const routes = [
  {path: '/home', element: <Home />},
  {path: '/about', element: <About />},
  {path: '/contact', element: <Contact />},
];

export default routes;
