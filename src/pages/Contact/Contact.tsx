import React from 'react';
import {makeStyles, createStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
}));
const Contact = () => {
  const classes = useStyles();
  return (
    <div>
      <h1 className={classes.root}>Contact</h1>
    </div>
  );
};

export default Contact;
