import React from 'react';
// import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
// import {Link} from 'react-router-dom';
import {Menu} from '@material-ui/core';
import {MenuItem} from '@mui/material';

// const useStyles = makeStyles((theme: Theme) => createStyles({
//   root: {
//
//   },
// }));

const MainMenu = React.forwardRef((props, ref) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  React.useImperativeHandle(ref, () => ({
    toggleMenu: (event: React.MouseEvent<HTMLButtonElement>) => {
      handleClick(event);
    },
  }));

  return (
    <div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>Profile</MenuItem>
        <MenuItem onClick={handleClose}>My account</MenuItem>
        <MenuItem onClick={handleClose}>Logout</MenuItem>
      </Menu>
    </div>
  );
});

export default MainMenu;
