import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {Home, Portrait, Mail} from '@material-ui/icons';
import {Link} from 'react-router-dom';

type Anchor = 'top' | 'left' | 'bottom' | 'right';

const LeftNav = React.forwardRef((props, ref) => {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor: Anchor, open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
    if (
      event.type === 'keydown'
        && ((event as React.KeyboardEvent).key === 'Tab'
            || (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }
    setState({...state, [anchor]: open});
  };

  React.useImperativeHandle(ref, () => ({
    toggleNavDrawer: (
      anchor: Anchor,
      open: boolean,
      event: React.KeyboardEvent | React.MouseEvent,
    ) => {
      toggleDrawer(anchor, open)(event);
    },
  }));

  const menuList = (anchor: Anchor) => (
    <List>
      {[{text: 'Home', icon: <Home />}, {text: 'About', icon: <Portrait />}, {text: 'Contact', icon: <Mail />}].map((button) => (
        <ListItem button component={Link} to={`/${button.text.toLowerCase()}`} onClick={toggleDrawer(anchor, false)} key={button.text}>
          <ListItemIcon>{button.icon}</ListItemIcon>
          <ListItemText primary={button.text} />
        </ListItem>
      ))}
    </List>
  );

  return (
    <div>
      {(['left', 'right', 'top', 'bottom'] as Anchor[]).map((anchor) => (
        <React.Fragment key={anchor}>
          <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
            {menuList('left')}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
});

export default LeftNav;
