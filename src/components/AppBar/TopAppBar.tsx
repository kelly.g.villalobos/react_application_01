import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from '@material-ui/core/Avatar';
import LeftNav from '../LeftNav/LeftNav';
import wolf from '../../resources/wolf.png';
import MainMenu from '../MainMenu/MainMenu';

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  large: {
    width: theme.spacing(6),
    height: theme.spacing(6),
  },
}));

const TopAppBar = () => {
  const classes = useStyles();
  const triggerLeftNav = React.useRef<any>(null);
  const triggerMenu = React.useRef<any>(null);

  const handleLoginClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    triggerMenu.current.toggleMenu(event);
  };

  const handleNavClick = (event: React.KeyboardEvent | React.MouseEvent) => {
    triggerLeftNav.current.toggleNavDrawer('left', true, event);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            onClick={handleNavClick}
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            News
          </Typography>
          <Avatar src={wolf} className={classes.large} />
          <Button color="inherit" onClick={handleLoginClick}>Login</Button>
          <MainMenu ref={triggerMenu} />
        </Toolbar>
      </AppBar>
      <LeftNav ref={triggerLeftNav} />
    </div>
  );
};

export default TopAppBar;
